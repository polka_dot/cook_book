import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import {createTheme, MuiThemeProvider} from '@material-ui/core/styles'
import defaultTheme from './assets/jsons/config.json'

const setJson = () => {
    setTheme(defaultTheme)
}

const setTheme = (json) => {
    const palette = {
        stateColor: {
            defaultColor: {
                default: json.palette.stateColor.defaultColor.default,
                bgPrimary: json.palette.stateColor.defaultColor.bgPrimary,
                bgSecondary: json.palette.stateColor.defaultColor.bgSecondary,
                bgSuccess: json.palette.stateColor.defaultColor.bgSuccess,
                bgInfo: json.palette.stateColor.defaultColor.bgInfo,
                bgWarning: json.palette.stateColor.defaultColor.bgWarning,
                bgDanger: json.palette.stateColor.defaultColor.bgDanger,
                bgLight: json.palette.stateColor.defaultColor.bgLight,
                bgDark: json.palette.stateColor.defaultColor.bgDark,
                bgWhite: json.palette.stateColor.defaultColor.bgWhite,
                bgBlack: json.palette.stateColor.defaultColor.bgBlack,
                bgGrey: json.palette.stateColor.defaultColor.bgGrey,
            },
            colors: {
                primary: {
                    base0: json.palette.stateColor.colors.primary.base0,
                    base1: json.palette.stateColor.colors.primary.base1,
                    base2: json.palette.stateColor.colors.primary.base2,
                    base3: json.palette.stateColor.colors.primary.base3,
                    base4: json.palette.stateColor.colors.primary.base4,
                    base5: json.palette.stateColor.colors.primary.base5,
                    base6: json.palette.stateColor.colors.primary.base6,
                    base7: json.palette.stateColor.colors.primary.base7,
                    base8: json.palette.stateColor.colors.primary.base8,
                    base9: json.palette.stateColor.colors.primary.base9,
                    base10: json.palette.stateColor.colors.primary.base10,
                    base11: json.palette.stateColor.colors.primary.base11,
                    base12: json.palette.stateColor.colors.primary.base12,
                }
            }
        }
    }
    const theme = createTheme({
        palette,
        breakpoints: {
            values: {
                xs: 0,
                sm: 600,
                md: 960,
                lg: 1280,
                xl: 1920,
            }
        },
        mixins: {
            pixelToRem: (pixel, pixelStart = 16) => `${Math.round((pixel / pixelStart) * 1000) / 1000}rem`
        },
        typography: {
            useNextVariants: true,
        },
        overrides: {
            MuiTableCell: {
                head: {
                    background: '#94948b',
                }
            }
        }
    })
    const MOUNT_NODE = document.getElementById('root')
    ReactDOM.render(
        <MuiThemeProvider theme={theme}>
            <App />
        </MuiThemeProvider>,
        MOUNT_NODE
    )
}

setJson()
