import en from '../../translations/en.json'
import pl from '../../translations/pl.json'

export default {
  DEFAULT_LANG: 'pl',
  AVAILABLE_LANGUAGES: ['pl', 'en'],
  LANG: {
    en,
    pl,
  },
  LOCALE_COOKIE_NAME: 'locale',
}
