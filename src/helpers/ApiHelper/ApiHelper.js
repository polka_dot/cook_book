export default {
    async request(method, url, data = null) {
        const headers = {
            'Content-Type': 'application/json',
        }
        return await fetch(url, { headers, method, body: data && JSON.stringify(data)}).then(res => res.json())
    }
}
