import TranslationsEnum from '../../enums/TranslationsEnum/TranslationsEnum'
import LocalStorageHelper from '../LocalStorageHelper/LocalStorageHelper'
import LangHelper from './LangHelper'

export default {
  setJSONLanguage: (lang) => {
    Object.keys(lang).map((el) => el === LocalStorageHelper.get('lang') && LocalStorageHelper.set('translation', lang[el]))
  },
  setDefaultLanguage: () => {
    const lang = TranslationsEnum.LANG
    const defaultLang = TranslationsEnum.DEFAULT_LANG
    LocalStorageHelper.set('lang', defaultLang)
    LangHelper.setJSONLanguage(lang)
  },
  changeLang: (langChange) => {
    const lang = TranslationsEnum.LANG
    LocalStorageHelper.set('lang', langChange)
    LangHelper.setJSONLanguage(lang)
  },
}
