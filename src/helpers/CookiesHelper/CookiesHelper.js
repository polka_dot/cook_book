export default {
  getCookie: (name) => {
    let dc = document.cookie
    let end
    let prefix = `${name}=`
    let begin = dc.indexOf('; ' + prefix)
    if (begin === -1) {
      begin = dc.indexOf(prefix)
      if (begin !== 0) return null
    } else {
      begin += 2
      end = dc.indexOf(';', begin)
      if (end === -1) end = dc.length
    }
    return decodeURI(dc.substring(begin + prefix.length, end))
  },
  setCookie: (key, name) => document.cookie = `${key}=${name}`,
}
