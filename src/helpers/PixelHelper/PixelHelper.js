export default {
    convertPixelToRem(px) {
        const defaultPixel = 16
        const rem = px / defaultPixel
        return rem + 'rem'
    }
}