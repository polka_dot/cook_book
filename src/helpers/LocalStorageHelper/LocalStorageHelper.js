import LocalStorageHelper from './LocalStorageHelper'

export default {
  isLocalStorageAvailable: () => {
    try {
      localStorage.setItem('a', 'b')
      localStorage.removeItem('a')
      return true
    } catch (e) {
      return console.log(e)
    }
  },
  set: (key, data) => {
    if (LocalStorageHelper.isLocalStorageAvailable())
      localStorage.setItem(key, JSON.stringify(data))
  },
  get: (key) => {
    if (LocalStorageHelper.isLocalStorageAvailable()) {
      try {
        return JSON.parse(localStorage.getItem(key))
      } catch (e) {
        return console.log(e)
      }
    }
  },
  remove: (key) => {
    if (LocalStorageHelper.isLocalStorageAvailable()) localStorage.removeItem(key)
  },
  clear: () => {
    if (LocalStorageHelper.isLocalStorageAvailable()) localStorage.clear()
  },
}
