import React from 'react'
import {Inbox} from '@material-ui/icons'
import P from '../../components/atoms/P/P'

export default {
    setRowElement: (text, label) => {
        if (label === 'duration') {
            const number = Number(text)
            if (number <= 30) {
                return <P color="green">{text + ' min'}</P>
            }
            return <P color="red">{text + ' min'}</P>
        } else {
            switch (text) {
                case 'no':
                    return <Inbox />
                case 'yes':
                    return <Inbox />
                default:
                    return text
            }
        }
    },
    setColumnLabel: (name) => {
        if (name) {
            switch (name) {
                case 'id':
                    return 'ID'
                case 'name':
                    return 'Imie i nazwisko'
                default:
                    return name
            }
        }
        return 'Brak nazwy kolumny'
    }
}
