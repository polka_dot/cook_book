import ValidationHelper from './ValidationHelper'
import _ from 'underscore'
import FormHelper from '../FormHelper/FormHelper'

export default {
    validText: (value) => !!value,
    validEmail: (value) => String(value).search(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/) !== -1,
    validPassword: (value) => String(value).search(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,}$/) !== -1,
    validHidden: () => true,
    validField: (type, value, name, state) => {
        let valid
        switch (type) {
            case 'text':
                valid = ValidationHelper.validText(value)
                break
            case 'email':
                valid = ValidationHelper.validEmail(value)
                break
            case 'password':
                valid = ValidationHelper.validPassword(value)
                if (valid && name === 'passwordSecond') {
                    const valuePassword = _.findWhere(state, {name: 'password'}).value
                    valid = valuePassword === value
                }
                break
            case 'hidden':
                valid = ValidationHelper.validHidden(value)
                break
            default:
                valid = ValidationHelper.validHidden(value)
                break
        }
        return valid
    },
    getErrorMessage: (type) => {
        if (type === 'text') return 'Brak wpisanej wartosci.'
        if (type === 'email') return 'Email niepoprawnie wpisany.'
        if (type === 'password') return 'Nieprawidlowe haslo.'
    },
    handleChange: (e, setButtonSubmit, state) => {
        const { name, type, value } = e.target
        if (name && type && value) {
            _.findWhere(state, {name: name}).value = value
            _.findWhere(state, {name: name}).valid = !!ValidationHelper.validField(type, value, name, state)
            console.log(state)
            setButtonSubmit(FormHelper.submitForm(state, setButtonSubmit))
        }
    }
}
