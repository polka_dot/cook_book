export default {
    changeStringToStars: (string) => Array(string.length).join('*')
}
