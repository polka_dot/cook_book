export default {
    submitForm: (state) => {
        const allElements = state.length
        let validElements = 0
        state.map((elem) => {
            if (elem.valid) {
              validElements += 1
              return true
            }
            return false
        })
        return validElements !== allElements
    }
}