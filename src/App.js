import React from 'react'
import {BrowserRouter as Router, Switch} from 'react-router-dom'
import {PublicRoute} from './router/PublicRoute/PublicRoute'
import {Redirect} from 'react-router'
import MainLayout from './layouts/MainLayout/MainLayout'
import LoginPage from './pages/LoginPage/LoginPage'
import DashboardPage from './pages/DashboardPage/DashboardPage'
import {PrivateRoute} from './router/PrivateRoute/PrivateRoute'
import LangHelper from './helpers/LangHelper/LangHelper'
import SimpleLayout from './layouts/SimpleLayout/SimpleLayout'
import AdminPage from './pages/AdminPage/AdminPage'
import RecipeOverviewPage from './pages/RecipeOverviewPage/RecipeOverviewPage'
import AddRecipePage from './pages/AddRecipePage/AddRecipePage'
import FavouritesPage from './pages/FavouritesPage/FavouritesPage'
import RecipePage from './pages/RecipePage/RecipePage'
import UsersPage from './pages/UsersPage/UsersPage'
import RegisterPage from './pages/RegisterPage/RegisterPage'

const App = () => {
    LangHelper.setDefaultLanguage()
    return (
        <Router basename={'/'}>
            <Switch>
                <PublicRoute exact path={'/'} component={()=> (<Redirect to={'/login'}/>)} layout={SimpleLayout} />
                <PublicRoute path={'/login'} component={LoginPage} layout={SimpleLayout} />
                <PublicRoute path={'/login/:action'} component={LoginPage} layout={SimpleLayout} />
                <PublicRoute path={'/register'} component={RegisterPage} layout={SimpleLayout} />
                <PublicRoute path={'/overview'} component={RecipeOverviewPage} layout={MainLayout} />
                <PublicRoute path={'/singleRecipe/:id'} component={RecipePage} layout={SimpleLayout} />
                <PrivateRoute path={'/addNew'} component={AddRecipePage} layout={MainLayout} />
                <PrivateRoute path={'/favourites'} component={FavouritesPage} layout={MainLayout} />
                <PrivateRoute path={'/users'} component={UsersPage} layout={MainLayout} />
                <PrivateRoute path={'/dashboard'} component={DashboardPage} layout={MainLayout} />
                <PrivateRoute path={'/admin'} component={AdminPage} layout={MainLayout} />
            </Switch>
        </Router>
    )
}

export default App
