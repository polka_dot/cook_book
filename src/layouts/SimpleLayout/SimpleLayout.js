import React from 'react'
import PropTypes from 'prop-types'

const SimpleLayout = (props) => {
    const Component = props.component
    const {location} = props

    return (
        <div style={{padding: 20}}>
            <Component location={location}/>
        </div>
    )
}

SimpleLayout.defaultProps = {
    location: false,
}

SimpleLayout.propsType = {
    location: PropTypes.object,
}

export default SimpleLayout