import React from 'react'
import PropTypes from 'prop-types'
import Navigation from '../../components/molecules/Navigation/Navigation'

const MainLayout = (props) => {
    const Component = props.component
    const {location} = props
    return (
        <div style={{padding: 20}}>
            <Navigation>
                <Component location={location}/>
            </Navigation>
        </div>
    )
}

MainLayout.defaultProps = {
    location: false,
}

MainLayout.propsType = {
    location: PropTypes.object,
}

export default MainLayout