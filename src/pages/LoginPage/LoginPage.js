import React, {useEffect, useState} from 'react'
import Wrapper from '../../components/atoms/Wrapper/Wrapper'
import {Grid, Container, makeStyles} from '@material-ui/core'
import Input from '../../components/atoms/Input/Input'
import Button from '../../components/atoms/Button/Button'
import uuid from 'react-uuid'
import _ from 'underscore'
import ValidationHelper from '../../helpers/ValidationHelper/ValidationHelper'
import LocalStorageHelper from '../../helpers/LocalStorageHelper/LocalStorageHelper'
import ApiHelper from '../../helpers/ApiHelper/ApiHelper'
import Snackbar from '../../components/molecules/Snackbars/Snackbars'


const useStyles = makeStyles(() => ({
    main: {
        padding: '0 !important',
    },
    container: {
        flexDirection: 'wrap',
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'baseline',
        maxWidth: 350,
        margin: '0 auto',
        justifyContent: 'center',
        padding: '50px 0',
        boxShadow: '0 0 20px 0 rgba(0, 0, 0, .15)',
    },
}))

const LoginPage = (props) => {
    const [ snackBarOpen, setSnackBarOpen] = useState(false)
    const [ snackBarVariant, setSnackBarVariant] = useState('success')
    const [ snackBarText, setSnackBarText] = useState(null)
    const t = LocalStorageHelper.get('translation')
    const classes = useStyles()
    const [ buttonSubmit, setButtonSubmit ] = useState(true)
    const [ state ] = useState([
        {
            name: 'login',
            type: 'email',
            required: true,
            label: 'Login',
            value: '',
            valid: null,
        },
        {
            name: 'password',
            type: 'password',
            required: true,
            label: 'Haslo',
            value: '',
            valid: null,
        }
    ])
    const showSnackBar = (text, variant= 'success') => {
        setSnackBarVariant(variant)
        setSnackBarText(text)
        setSnackBarOpen(true)
    }
    const closeSnackBar = () => {
        setSnackBarText('')
        setSnackBarOpen(false)
    }
    const handleSubmit = () => {
        const email =  _.findWhere(state, {name: 'login'}).value
        const password =  _.findWhere(state, {name: 'password'}).value
        ApiHelper.request('get', `//141.95.55.206/api/users/getUsers.php?email=${email}&password=${password}`).then(res => {
           if (res) {
               if (res[0].active === '1') {
                   LocalStorageHelper.set('user', res)
                   props.location.history.push('/dashboard')
               } else {
                   showSnackBar('Przepraszamy, Twoj uzytkownik nie jest aktywowany :-(', 'warning')
               }
           }
        }).catch(e => {
            showSnackBar('Przepraszamy, Twoje haslo lub login jest niepoprawne', 'error')
        })
    }
    // useEffect(() => {
    //     if (props.location.location.state.success) {
    //         showSnackBar('Konto zostalo poprawnie zarejestrowane, ale czeka na aktywacje.', 'success')
    //     }
    // }, [])
    return (
       <>
           <Grid container className={classes.container}>
               <Grid item xs={12}>
                   <Container component="main" className={classes.main}>
                       <div className={classes.paper}>
                           <Wrapper title={t.loginPage.title} column>
                               {
                                   state && state.map((el) => {
                                       return (
                                           <Input
                                               key={uuid()}
                                               name={el.name}
                                               type={el.type}
                                               value={el.value}
                                               required={el.required}
                                               label={el.label}
                                               onChange={(e) => ValidationHelper.handleChange(e, setButtonSubmit, state)}
                                           />
                                       )
                                   })
                               }
                               <Button type="button" onClick={() => handleSubmit()} disabled={buttonSubmit}>Zaloguj</Button>
                               <Button type="button" onClick={() => props.location.history.push('/register')}>Zarejestruj</Button>
                           </Wrapper>
                       </div>
                   </Container>
               </Grid>
           </Grid>
           {
               snackBarOpen && (
                   <Snackbar
                       text={snackBarText}
                       open={true}
                       callback={closeSnackBar}
                       variant={snackBarVariant}
                   />
               )
           }
       </>
    )
}

export default LoginPage
