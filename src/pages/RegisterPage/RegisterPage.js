import React, {useState} from 'react'
import {Container, Grid, makeStyles} from '@material-ui/core'
import Wrapper from '../../components/atoms/Wrapper/Wrapper'
import LocalStorageHelper from '../../helpers/LocalStorageHelper/LocalStorageHelper'
import Button from '../../components/atoms/Button/Button'
import Input from '../../components/atoms/Input/Input'
import uuid from 'react-uuid'
import ValidationHelper from '../../helpers/ValidationHelper/ValidationHelper'
import _ from 'underscore'
import ApiHelper from '../../helpers/ApiHelper/ApiHelper'
import Snackbar from '../../components/molecules/Snackbars/Snackbars'

const useStyles = makeStyles(() => ({
    main: {
        padding: '0 !important',
    },
    container: {
        flexDirection: 'wrap',
    },
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'baseline',
        maxWidth: 350,
        margin: '0 auto',
        justifyContent: 'center',
        padding: '50px 0',
        boxShadow: '0 0 20px 0 rgba(0, 0, 0, .15)',
    },
}))

const RegisterPage = (props) => {
    const classes = useStyles()
    const t = LocalStorageHelper.get('translation')
    const [ buttonSubmit, setButtonSubmit ] = useState(true)
    const [ password, setPassword ] = useState(null)
    const [ snackBarOpen, setSnackBarOpen] = useState(false)
    const [ snackBarVariant, setSnackBarVariant] = useState('success')
    const [ snackBarText, setSnackBarText] = useState(null)
    const showSnackBar = (text, variant= 'success') => {
        setSnackBarVariant(variant)
        setSnackBarText(text)
        setSnackBarOpen(true)
    }
    const closeSnackBar = () => {
        setSnackBarText('')
        setSnackBarOpen(false)
    }
    const [ state ] = useState([
        {
            name: 'login',
            type: 'email',
            required: true,
            label: 'Login',
            value: '',
            valid: null,
        },
        {
            name: 'password',
            type: 'password',
            required: true,
            label: 'Hasło',
            value: '',
            valid: null,
        },
        {
            name: 'passwordSecond',
            type: 'password',
            required: true,
            label: 'Powtórz hasło',
            value: '',
            valid: null,
        },
        {
            name: 'name',
            type: 'text',
            required: true,
            label: 'Imie i nazwisko',
            value: '',
            valid: null,
        },
        {
            name: 'city',
            type: 'text',
            required: true,
            label: 'Miasto',
            value: '',
            valid: null,
        }
    ])
    const handleRegister = () => {
        const data = {
            login: _.findWhere(state, {name: 'login'}).value,
            password: _.findWhere(state, {name: 'password'}).value,
            passwordSecond: _.findWhere(state, {name: 'passwordSecond'}).value,
            name: _.findWhere(state, {name: 'name'}).value,
            city: _.findWhere(state, {name: 'city'}).value,
        }
       ApiHelper.request('get', `http://localhost/cook_book/api/users/setUser.php?login=${data.login}&password=${data.password}&name=${data.name}&city=${data.city}`, null).then((res) => {
           setButtonSubmit(true)
           if (res) {
               props.location.history.push({
                   pathname: '/login',
                   state: {
                       success: true,
                   },
               })
           } else {
               showSnackBar('Przepraszamy, Ten uzytkownik juz istnieje', 'error')
           }
       })
    }
    return (
        <>
            <Grid container className={classes.container}>
                <Grid item xs={12}>
                    <Container component="main" className={classes.main}>
                        <div className={classes.paper}>
                            <Wrapper title={t.registerPage.title} column>
                                {
                                    state && state.map((el) => {
                                        return (
                                            <Input
                                                key={uuid()}
                                                name={el.name}
                                                type={el.type}
                                                value={el.value}
                                                required={el.required}
                                                label={el.label}
                                                onChange={(e) => ValidationHelper.handleChange(e, setButtonSubmit, state)}
                                            />
                                        )
                                    })
                                }
                                <Button type="button" onClick={() => handleRegister()} disabled={buttonSubmit}>Zarejestruj</Button>
                                <Button type="button" onClick={() => props.location.history.push('/login')}>Zaloguj</Button>
                            </Wrapper>
                        </div>
                    </Container>
                </Grid>
            </Grid>
            {
                snackBarOpen && (
                    <Snackbar
                        text={snackBarText}
                        open={true}
                        callback={closeSnackBar}
                        variant={snackBarVariant}
                    />
                )
            }
        </>
    )
}

export default RegisterPage
