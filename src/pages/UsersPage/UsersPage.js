import React, {useEffect, useState} from 'react'
import ApiHelper from '../../helpers/ApiHelper/ApiHelper'
import LocalStorageHelper from '../../helpers/LocalStorageHelper/LocalStorageHelper'
import UserWrapper from '../../components/molecules/UserWrapper/UserWrapper'
import {Grid} from '@material-ui/core'

const UsersPage = () => {
    const [ data, setData ] = useState(null)
    const [ update, setUpdate ] = useState(false)
    const handleData = () => {
        ApiHelper.request('get', 'http://localhost/cook_book/api/users/getUsers.php?getAll=true').then(res => {
            if (res) {
                setData(res)
            }
        })
    }
    useEffect(() => {
        handleData()
    },[])
    useEffect(() => {
        handleData()
    },[update])
    return (
        <Grid container spacing={3}>
            {
                data && Object.values(data).map((el, index) => (
                  <Grid
                    item
                    md={4}
                    key={index}
                  >
                      <UserWrapper
                          update={update}
                          setUpdate={setUpdate}
                          active={el.active}
                          email={el.login}
                          role={el.role}
                          password={el.password}
                          id={el.id}
                          name={el.name}
                      />
                  </Grid>

                ))
            }
        </Grid>
    )
}

export default UsersPage
