import React, {useEffect, useState} from 'react'
import {Grid} from '@material-ui/core'
import Wrapper from '../../components/atoms/Wrapper/Wrapper'
import TableHelper from '../../helpers/TableHelper/TableHelper'
import Tables from '../../components/organisms/Table/Table'
import ApiHelper from '../../helpers/ApiHelper/ApiHelper'

const RecipeOverviewPage = () => {
    const [ data, setData ] = useState(null)
    const handleHeadRows = () => {
        const tempHeadRows = []
        if (data && data.length > 0) {
            Object.keys(data[0]).map(el => {
                tempHeadRows.push({
                    id: el,
                    order: el.toLowerCase() === 'id' ? 0 : 1,
                    numeric: false,
                    disabledPadding: false,
                    label: TableHelper.setColumnLabel(el)
                })
            })
        }
        return tempHeadRows
    }
    const headRows = handleHeadRows()
    console.log(headRows)
    const handleRows = () => {
        const columns = []
        const tempRows = []
        if (data && data.length > 0) {
            Object.keys(data[0]).map((el) => {
                columns.push(el)
            })
            data.map((el) => {
                const tempObject = {}
                Object.values(el).map((element, index) => {
                    tempObject[columns[index]] = element
                })
                tempRows.push(tempObject)
            })
        }
        return tempRows
    }
    const rows = handleRows()
    const handleData = () => {
        ApiHelper.request('get', '//localhost/cook_book/api/data/getData.php').then(res => {
            if (res) {
                setData(res)
            }
        }).catch(e => {
            console.log(e)
        })
    }
    useEffect(() => {
        handleData()
    },[])

    return (
        <Grid container>
            <Grid item lg={12}>
                <Wrapper column title='Lista Przepisow'>
                    {
                        data && data.length > 0 && (
                            <Tables headRows={headRows} rows={data} orderDefault={'id'} />
                        )
                    }
                </Wrapper>
            </Grid>
        </Grid>
    )
}

export default RecipeOverviewPage
