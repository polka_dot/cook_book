import React, {useEffect, useState} from 'react'
import {Grid} from '@material-ui/core'
import ApiHelper from '../../helpers/ApiHelper/ApiHelper'

const RecipePage = (props) => {
    const [ data, setData ] = useState(null)
    const handleData = (id) => {
        ApiHelper.request('get', `http://localhost/cook_book/api/recipes/id?id=${id}`, null).then(res => {
            if (res) {
                console.log(res)
            }
        })
    }
    useEffect(() => {
        const id = props.location.location.pathname.split('/')[2]
        handleData(id)
    }, [])
    return (
        <Grid container>
            Tutaj jest Recepta Mniami Mniami
        </Grid>
    )
}

export default RecipePage
