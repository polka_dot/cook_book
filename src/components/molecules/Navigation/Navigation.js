import React, {useState} from 'react'
import {
    AppBar,
    CssBaseline,
    Divider,
    Drawer,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    makeStyles,
    Toolbar,
    Typography,
    useTheme
} from '@material-ui/core'
import clsx from 'clsx'
import IconButton from '@material-ui/core/IconButton'
import {
    AddBox,
    ChevronLeft,
    ChevronRight,
    Dashboard,
    Fastfood,
    Inbox,
    MeetingRoom,
    Menu,
    SupervisorAccount
} from '@material-ui/icons'
import Link from '../../atoms/Link/Link'
import LocalStorageHelper from '../../../helpers/LocalStorageHelper/LocalStorageHelper'


const drawerWidth = 240
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    appBar: {
       transition: theme.transitions.create(['margin', 'width'], {
           easing: theme.transitions.easing.sharp,
           duration: theme.transitions.duration.leavingScreen,
       }),
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        marginTop: 70,
        padding: theme.spacing(1),
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    }
}))

const Navigation = ({ children }) => {
    const user = LocalStorageHelper.get('user')[0]
    const classes = useStyles()
    const theme = useTheme()
    const [ open, setOpen ] = useState(false)
    const handleDrawerOpen = () => setOpen(true)
    const handleDrawerClose = () => setOpen(false)
    const nav = [
        {
            icon: <Dashboard />,
            link: '/dashboard',
            text: 'Dashboard',
            role: 'user',
        },
        {
            icon: <SupervisorAccount />,
            link: '/admin',
            text: 'Admin',
            role: 'admin',
        },
        {
            icon: <Fastfood />,
            link: '/overview',
            text: 'Wszytskie Przepisy',
            role: 'user',
        },
        {
            icon: <AddBox />,
            link: '/addNew',
            text: 'Nowy przepis',
            role: 'admin',
        },
        {
            icon: <AddBox />,
            link: '/users',
            text: 'Panel Kontroli Uzytkownikow',
            role: 'admin',
        }
    ]
    const generateListItem = (role = 'user', text, icon, divider= true, link) => (
        <Link
            href={link}
        >
            <List>
                <ListItem button>
                    <ListItemIcon>
                        {icon}
                    </ListItemIcon>
                    <ListItemText primary={text} />
                </ListItem>
            </List>
            {
                divider && <Divider />
            }
        </Link>
    )
    const handleLogout = () => {
        LocalStorageHelper.clear()
        window.open('/login', '_self')
    }
    return (
        <div className={classes.root}>
            <CssBaseline />
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <Menu />
                    </IconButton>
                    <Typography
                        variant="h6"
                        noWrap
                    >
                        Drawer
                    </Typography>
                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton
                        onClick={handleDrawerClose}
                    >
                        {
                            theme.direction === 'ltr' ? <ChevronLeft /> : <ChevronRight />
                        }
                    </IconButton>
                </div>
                <Divider />
                {
                    nav.map((navItem, index) => (
                        <React.Fragment key={index}>
                            {((navItem.role === 'user' &&
                                (user.role === 'user' ||
                                    user.role === 'admin' ||
                                    user.role === 'superuser')) ||
                                (navItem.role === 'admin' &&
                                    (user.role === 'admin' || user.role === 'superuser')) ||
                                (navItem.role === 'superuser' &&
                                    user.role === 'superuser')) && (
                                generateListItem(navItem.role, navItem.text, navItem.icon, nav.length - 1 !== index, navItem.link)
                            )}
                        </React.Fragment>
                    ))
                }
                <List onClick={handleLogout}>
                    <ListItem button>
                        <ListItemIcon>
                            <MeetingRoom/>
                        </ListItemIcon>
                        <ListItemText primary="Wyloguj" />
                    </ListItem>
                </List>
             </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                })}
            >
                {children}
            </main>
        </div>
    )
}

export default Navigation
