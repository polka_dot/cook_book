import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import ErrorIcon from '@material-ui/icons/Error'
import InfoIcon from '@material-ui/icons/Info'
import WarningIcon from '@material-ui/icons/Warning'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import classNames from 'classnames'
import hexToRgba from 'hex-to-rgba'
import classesModule from './Snackbars.module.scss'

const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
}
const styles1 = (theme) => ({
    success: {
        backgroundColor:
            theme.palette.stateColor.defaultColor.bgSuccess + ' !important',
        color: theme.palette.stateColor.defaultColor.bgWhite + ' !important',
        border: hexToRgba(theme.palette.stateColor.defaultColor.bgSuccess, 0.2),
    },
    error: {
        backgroundColor:
            theme.palette.stateColor.defaultColor.bgDanger + ' !important',
        color: theme.palette.stateColor.defaultColor.bgWhite + ' !important',
        border: hexToRgba(theme.palette.stateColor.defaultColor.bgDanger, 0.2),
    },
    info: {
        backgroundColor:
            theme.palette.stateColor.defaultColor.bgInfo + ' !important',
        color: theme.palette.stateColor.defaultColor.bgWhite + ' !important',
        border: hexToRgba(theme.palette.stateColor.defaultColor.bgInfo, 0.2),
    },
    warning: {
        backgroundColor:
            theme.palette.stateColor.defaultColor.bgWarning + ' !important',
        color: theme.palette.stateColor.defaultColor.bgWhite + ' !important',
        border: hexToRgba(theme.palette.stateColor.defaultColor.bgWarning, 0.2),
    },
    icon: {
        fill: theme.palette.stateColor.defaultColor.bgWhite,
        fontSize: 20,
        marginRight: '10px',
    },
    iconVariant: {
        opacity: 0.9,
        fill: theme.palette.stateColor.defaultColor.bgWhite,
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    close: {
        border: 'none !important',
        fill: theme.palette.stateColor.defaultColor.bgWhite,
        opacity: 0.6,
        '&:hover': {
            opacity: 1,
        },
    },
    content: {
        width: '100%',
        flex: 1,
    },
    action: {
        paddingLeft: '6px',
    },
})

const MySnackbarContent = (props) => {
    const { classes, className, message, onClose, variant, ...other } = props
    const Icon = variantIcon[variant]

    return (
        <SnackbarContent
            className={classNames(
                classes[variant],
                className,
                classesModule['Snackbars']
            )}
            classes={{
                message: classes['content'],
                action: classes['action'],
            }}
            aria-describedby="client-snackbar"
            message={
                <span id="client-snackbar" className={classes.message}>
          <Icon className={classNames(classes.icon, classes.iconVariant)} />
                    {message}
        </span>
            }
            action={[
                <IconButton
                    key="close"
                    aria-label="Close"
                    color="inherit"
                    className={classes.close}
                    onClick={onClose}
                >
                    <CloseIcon className={classes.icon} />
                </IconButton>,
            ]}
            {...other}
        />
    )
}

MySnackbarContent.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    message: PropTypes.node,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
}
const MySnackbarContentWrapper = withStyles(styles1)(MySnackbarContent)

const styles = (theme) => ({
    close: {
        padding: '20px',
    },
})

class ConsecutiveSnackbars extends React.Component {
    constructor(props) {
        super(props)
        this.text = this.props.text
        this.background = this.props.background
        this.callback = this.props.callback
    }
    queue = [];
    state = {
        open: this.props.open ? this.props.open : false,
        messageInfo: {},
        variant: this.props.variant ? this.props.variant : 'success',
    };
    handleClick = (message) => () => {
        this.queue.push({
            message,
            key: new Date().getTime(),
        })
        if (this.state.open) {
            this.setState({ open: false })
        } else {
            this.processQueue()
        }
    };
    processQueue = () => {
        if (this.queue.length > 0) {
            this.setState({
                messageInfo: this.queue.shift(),
                open: true,
            })
        }
    };
    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }
        this.setState({ open: false })
        if (this.callback) this.callback()
    };
    handleExited = () => {
        this.processQueue()
    };

    render() {
        const { messageInfo } = this.state
        return (
            <React.Fragment>
                {this.props.children && (
                    <div onClick={this.text && this.handleClick(this.text)}>
                        {this.props.children}
                    </div>
                )}
                <Snackbar
                    key={messageInfo.key}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                    variant="success"
                    open={this.state.open}
                    autoHideDuration={6000}
                    onClose={this.handleClose}
                    onExited={this.handleExited}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{messageInfo.message}</span>}
                    className="Message"
                    action={[
                        <IconButton
                            key="close"
                            aria-label="Close"
                            color="white !important"
                            onClick={this.handleClose}
                        >
                            <CloseIcon />
                        </IconButton>,
                    ]}
                >
                    <MySnackbarContentWrapper
                        onClose={this.handleClose}
                        variant={this.state.variant}
                        message={this.text}
                    />
                </Snackbar>
            </React.Fragment>
        )
    }
}

ConsecutiveSnackbars.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(ConsecutiveSnackbars)
