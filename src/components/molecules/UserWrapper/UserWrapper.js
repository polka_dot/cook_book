import React, {useState} from 'react'
import Wrapper from '../../atoms/Wrapper/Wrapper'
import P from '../../atoms/P/P'
import StringHelper from '../../../helpers/StringHelper/StringHelper'
import LocalStorageHelper from '../../../helpers/LocalStorageHelper/LocalStorageHelper'
import {Check, Close} from '@material-ui/icons'
import {useTheme, withTheme} from '@material-ui/core'
import ApiHelper from '../../../helpers/ApiHelper/ApiHelper'

const UserWrapper = ({
    email,
    role,
    password,
    active,
    id,
    name,
    theme,
    update,
    setUpdate,
}) => {
    const [ pass, setPass ] = useState(StringHelper.changeStringToStars(password))
    const user = LocalStorageHelper.get('user')[0]?.role
    const styles = {
        green: {
            color: theme.palette.stateColor.defaultColor.bgSuccess,
            position: 'absolute',
            right: 50,
            top: 50,
        },
        red: {
            color: theme.palette.stateColor.defaultColor.bgDanger,
            position: 'absolute',
            right: 50,
            top: 50,
        },
    }
    const handleClick = () => {
        if (user === 'superuser') {
            if (pass.includes('*')) setPass(password)
            else setPass(StringHelper.changeStringToStars(password))
        }
    }
    const handleActive = (activate) => {
        if (user === 'superuser') {
            if (activate === 1) {
                ApiHelper.request('get', `http://localhost/cook_book/api/users/inactiveUser.php?id=${id}`, null)
                    .then(res => {
                        if (res) {
                            setUpdate(!update)
                        }
                })

            } else if (activate === 0) {
                ApiHelper.request('get', `http://localhost/cook_book/api/users/activeUser.php?id=${id}`, null)
                    .then(res => {
                        if (res) {
                            setUpdate(!update)
                        }
                    })
            }
        }
    }
    return (
        <Wrapper title={name} column>
            <P><b>E-mail:</b>&nbsp;{email}</P>
            <P cursor={user === 'superuser'} onClick={() => handleClick()}><b>Password:</b>&nbsp;{pass}</P>
            <P><b>Rola:</b>&nbsp;{role}</P>
            <div>
                {
                    active && active === '1' ?  <Check style={styles.green} onClick={() => handleActive(1)} /> : <Close style={styles.red}  onClick={() => handleActive(0)} />
                }
            </div>
        </Wrapper>
    )
}

export default withTheme(UserWrapper)
