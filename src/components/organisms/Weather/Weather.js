import React, {useEffect, useState} from 'react'
import LocalStorageHelper from '../../../helpers/LocalStorageHelper/LocalStorageHelper'
import P from '../../atoms/P/P'

const Weather = () => {
    const [ data, setData ] = useState(null)
    const [ apiCall, setApiCall ] = useState(null)
    const userData = LocalStorageHelper.get('user')
    const name = data && data.name
    const temp = data && data.main && data.main.temp
    const tempLike = data && data.main && data.main.feels_like
    const tempMin = data && data.main && data.main.temp_min
    const tempMax = data && data.main && data.main.temp_max
    const windDeg = data && data.wind && data.wind.deg
    const windSpeed = data && data.wind && data.wind.speed
    const kelvinToCelsius = (temp) => `${Math.round((temp - 273.15) * 100) / 100} ℃`
    const handleDegrees = (deg = 0) => {
        if (deg === 0) return '↑'
        if (deg > 0 && deg < 90) return '↗'
        if (deg === 90) return '→'
        if (deg > 90 && deg < 180) return '↘'
        if (deg === 180) return '↓'
        if (deg > 180 && deg < 270) return '↙'
        if (deg === 270) return '←'
        if (deg > 270 && deg < 360) return '↖'
        return '↑'
    }
    const handleWeather = async () => {
        await fetch(`${process.env.REACT_APP_WEATHER_URL}?q=${userData ? userData.city : ''}&appid=${process.env.REACT_APP_WEATHER_API}`)
            .then(res => {
                setApiCall(res.json())
            })
    }
    useEffect(() => {
        if (apiCall) {
            apiCall.then(res => {
                setData(res)
            })
        }
    }, [apiCall])

    useEffect(() => {
        handleWeather()
    }, [])
    return (
        <>
            <P><b>Miasto:</b> {name}</P>
            <P><b>Temperatura:</b> {kelvinToCelsius(temp)}</P>
            <P><b>Temperatura Odczuwalma:</b> {kelvinToCelsius(tempLike)}</P>
            <P><b>Temperatura Minimalna:</b> {kelvinToCelsius(tempMin)}</P>
            <P><b>Temperatura Maksymalna:</b> {kelvinToCelsius(tempMax)}</P>
            <P><b>Kierunek Wiatru:</b> {handleDegrees(windDeg)}</P>
            <P><b>Predkosc Wiatru:</b> {windSpeed} m/s</P>
        </>
    )
}

export default Weather