import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import TableSortLabel from '@material-ui/core/TableSortLabel'
import Paper from '@material-ui/core/Paper'
import TableHelper from '../../../helpers/TableHelper/TableHelper'
import BreakpointsHelper from '../../../helpers/BreakpointsHelper/BreakpointsHelper'
import ApiHelper from '../../../helpers/ApiHelper/ApiHelper'
import LocalStorageHelper from '../../../helpers/LocalStorageHelper/LocalStorageHelper'

const desc = (a, b, orderBy) => {
    if (b[orderBy] < a[orderBy]) return -1
    if (b[orderBy] > a[orderBy]) return 1
    return 0
}
const stableSort = (array, cmp) => {
    const stabilizedThis = array.map((el, index) => [el, index])
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0])
        if (order !== 0) return order
        return a[1] - b[1]
    })
    return stabilizedThis.map((el) => el[0])
}
const getSorting = (order, orderBy) => {
    return order === 'desc'
        ? (a, b) => desc(a, b, orderBy)
        : (a, b) => -desc(a, b, orderBy)
}
const EnhancedTableHead = ({
       order,
       orderBy,
       onRequestSort,
       headRows,
       actions,
}) => {
    const createSortHandler = (property) => (event) => onRequestSort(event, property)
    return (
        !BreakpointsHelper.isMobile() && (
            <TableHead>
                <TableRow>
                    {actions && (
                        <TableCell align={'left'} sortDirection={false}>
                            <TableSortLabel direction={order}>Operacje</TableSortLabel>
                        </TableCell>
                    )}
                    {headRows.map((row) => (
                        <TableCell
                            key={row.id}
                            align={row.numeric ? 'right' : 'left'}
                            padding={row.disablePadding ? 'none' : 'normal'}
                            sortDirection={orderBy === row.id ? order : false}
                        >
                            <TableSortLabel
                                active={orderBy === row.id}
                                direction={order}
                                onClick={createSortHandler(row.id)}
                            >
                                {row.label}
                            </TableSortLabel>
                        </TableCell>
                    ))}
                </TableRow>
            </TableHead>
        )
    )
}
EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
        boxShadow: BreakpointsHelper.isMobile() && 'none',
    },
    table: {
        minWidth: 750,
        margin: 20,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
}))
const Tables = ({ headRows, rows, actions, orderDefault }) => {
    const classes = useStyles()
    const [order, setOrder] = useState('asc')
    const [orderBy, setOrderBy] = useState(orderDefault)
    const [selected, setSelected] = useState([])
    const [page, setPage] = useState(0)
    const [rowsPerPage, setRowsPerPage] = useState(15)
    const handleRequestSort = (event, property) => {
        setOrder(orderBy === property && order === 'desc' ? 'asc' : 'desc')
        setOrderBy(property)
    }
    const handleSelectAllClick = (event) => {
        if (event.target.checked) setSelected(rows.map((n) => n.name))
        setSelected([])
    }
    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name)
        let newSelected = []
        if (selectedIndex === -1) newSelected = newSelected.concat(selected, name)
        else if (selectedIndex === 0) newSelected = newSelected.concat(selected.slice(1))
        else if (selectedIndex === selected.length - 1) newSelected = newSelected.concat(selected.slice(0, -1))
        else if (selectedIndex > 0) newSelected = newSelected.concat(selected.slice(0, selectedIndex), selected.slice(selectedIndex + 1))
        setSelected(newSelected)
    }
    const style = {
        rowMobile: {
            display: 'flex',
            flexDirection: 'column',
            marginBottom: '20px',
            border: '1px solid rgba(224, 224, 224, 1)',
        },
        cellMobile: {
            border: 'none',
            padding: '5px 10px',
            display: 'block',
            height: 'auto',
        },
        headCell: {
            fontWeight: '600',
        },
        bodyMobile: {
            display: 'block',
        },
        tableMobile: {
            minWidth: 0,
            margin: 0,
            width: '100%',
            display: 'block',
        },
    }
    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <div className={classes.tableWrapper}>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size="medium"
                        style={BreakpointsHelper.isMobile() ? style.tableMobile : {}}
                    >
                        <EnhancedTableHead
                            numSelected={selected.length}
                            order={order}
                            orderBy={orderBy}
                            onSelectAllClick={handleSelectAllClick}
                            onRequestSort={handleRequestSort}
                            rowCount={rows.length}
                            headRows={headRows}
                            actions={actions}
                        />
                        <TableBody
                            style={BreakpointsHelper.isMobile() ? style.bodyMobile : {}}
                        >
                            {
                                stableSort(rows, getSorting(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((row, index) => {
                                        const isItemSelected = selected.indexOf(row.name) !== -1
                                        return (
                                            <TableRow
                                                key={index}
                                                hover
                                                onClick={(event) => handleClick(event, row.name)}
                                                role="checkbox"
                                                aria-checked={isItemSelected}
                                                style={BreakpointsHelper.isMobile() ? style.rowMobile : {}}
                                            >
                                                { actions && <TableCell>{actions}</TableCell> }
                                                {
                                                    Object.values(row).map((el, index) => {
                                                        return (
                                                            <TableCell
                                                                key={index}
                                                                style={ BreakpointsHelper.isMobile() ? style.cellMobile : {} }
                                                            >
                                                                {
                                                                    BreakpointsHelper.isMobile() && (
                                                                        <div style={style.headCell}>
                                                                            {`${headRows[index].label}: `}
                                                                        </div>
                                                                    )
                                                                }
                                                                {
                                                                    TableHelper.setRowElement(el, headRows[index].label) || '-'
                                                                }
                                                            </TableCell>
                                                        )
                                                    })}
                                            </TableRow>
                                        )
                                    })}
                        </TableBody>
                    </Table>
                </div>
                <TablePagination
                    rowsPerPageOptions={[15, 50, 100]}
                    component="div"
                    count={rows.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    backIconButtonProps={{
                        'aria-label': 'Poprzednia strona',
                    }}
                    nextIconButtonProps={{
                        'aria-label': 'Następna strona',
                    }}
                    onPageChange={(event, newPage) => setPage(newPage)}
                    onRowsPerPageChange={(event) => setRowsPerPage(+event.target.value)}
                />
            </Paper>
        </div>
    )
}

Tables.propTypes = {
    orderDefault: 'id',
}

Tables.propTypes = {
    headRows: PropTypes.array.isRequired,
    orderDefault: PropTypes.string,
}

export default Tables
