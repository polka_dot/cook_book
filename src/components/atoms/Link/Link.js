import React from 'react'
import LinkMaterial from '@material-ui/core/Link'
import PropTypes from 'prop-types'

const Link = ({
    href,
    target,
    title,
    rel,
    children,
}) => {
    return (
        <LinkMaterial
            href={href}
            target={target}
            title={title}
            rel={rel}
        >
            {children}
        </LinkMaterial>
    )
}

Link.defaultProps = {
    target: '_self',
    title: '',
    rel: 'noreferrer',
}

Link.propTypes = {
    href: PropTypes.string.isRequired,
    target: PropTypes.string,
    title: PropTypes.string,
    rel: PropTypes.string,
    children: PropTypes.any.isRequired,
}

export default Link
