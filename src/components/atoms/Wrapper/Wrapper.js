import React, {useEffect} from 'react'
import {withTheme} from '@material-ui/core'
import PixelHelper from '../../../helpers/PixelHelper/PixelHelper'
import P from '../P/P'

const Wrapper = (props) => {
    const {
        scroll,
        theme,
        borderRadius,
        column,
        title,
    } = props

    const style = {
        root: {
            borderRadius: borderRadius || PixelHelper.convertPixelToRem(20),
            backgroundColor: theme.palette.stateColor.defaultColor.bgWhite,
            display: 'flex',
            flexDirection: column ? 'column' : 'row',
            padding: '50px',
            position: 'relative',
        },
        title: {
            textTransform: 'uppercase',
        },
        children: {
            display: 'flex',
            flexDirection: 'column',
            overflowY: scroll && 'auto',
            maxHeight: scroll && 'calc(100vh - 300px)',
        },
    }
    const abc = () => {
        const absbsbs = [1,2, 3]
        absbsbs.map(idx => {

        })
    }
    useEffect(() => {
        abc()
    }, [])
    return (
        <div style={style.root}>
            {
                title && (
                    <div style={style.title}>
                        <P fontWeight={600}>
                            {title}
                        </P>
                    </div>
                )
            }
            <div style={style.children}>
                {props.children}
            </div>
        </div>
    )
}

export default withTheme(Wrapper)
