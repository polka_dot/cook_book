import React from 'react'

const P = (props) => {
    const {
        flexDirection,
        alignItems,
        borderBottom,
        fontWeight,
        fontSize,
        letterSpacing,
        width,
        color,
        onClick,
        cursor,
    } = props

    const style = {
        root: {
            flexDirection,
            alignItems,
            borderBottom,
            fontWeight: fontWeight || 400,
            fontSize: fontSize || 13,
            letterSpacing: letterSpacing || '.7px',
            width: width || '100%',
            fontFamily: 'Roboto, serif,',
            color,
            cursor: cursor && 'pointer',
        }
    }

    return (
       <div style={style.root} onClick={onClick}>
           {props.children}
       </div>
    )
}

export default P
