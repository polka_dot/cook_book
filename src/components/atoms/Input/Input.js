import React, {useState} from 'react'
import {FormControl, TextField, withStyles} from '@material-ui/core'
import uuid from 'react-uuid'
import ValidationHelper from '../../../helpers/ValidationHelper/ValidationHelper'
import PixelHelper from '../../../helpers/PixelHelper/PixelHelper'
import PropTypes from 'prop-types'
import P from '../P/P'

const styles = theme => ({
    textFieldError: {
        width: '100%',
        border: `1px solid ${theme.palette.stateColor.defaultColor.bgDanger}`,
    },
    textFieldTrue: {
        width: '100%',
        border: `1px solid ${theme.palette.stateColor.defaultColor.bgPrimary}`,
        marginTop: PixelHelper.convertPixelToRem(10),
    },
})

const Input = ({ type, name, label, placeholder, value, required, disabled, readOnly, autoFocus, onChange, classes, marginTop }) => {
    const style = {
        root: {
            marginTop,
        }
    }
    const [ error, setError ] = useState(false)
    const [ errorMessage, setErrorMessage ] = useState(false)

    const handleOnChange = (e) => {
        if (onChange) onChange(e)
        const val = e.target.value
        const valid = ValidationHelper.validField(type, val)
        if (!valid) {
            setError(true)
            setErrorMessage(ValidationHelper.getErrorMessage(type))
        } else {
            setError(false)
            setErrorMessage('')
        }
    }

    return (
        <FormControl>
            <TextField
                style={style.root}
                fullWidth
                id={uuid()}
                type={type}
                name={name}
                label={label}
                placeholder={placeholder}
                error={error}
                defaultValue={value}
                onChange={(e) => handleOnChange(e)}
                className={error ? classes.textFieldError : classes.textFieldTrue}
                required={required}
                disabled={disabled}
                variant="outlined"
                inputProps={{
                    autoComplete: 'off',
                }}
                readOnly={readOnly}
                autoFocus={autoFocus}
            />
            {
                type === 'password' && (
                    <P>
                        Haslo powinno zawierac jedna duza litere, jedna liczbe i znak specjalny i miec minimum 8 znakow
                    </P>
                )
            }
            {
                error && (
                    <P>
                        { errorMessage }
                    </P>
                )
            }
        </FormControl>
    )
}

Input.defaultProps = {
    type: 'text',
    label: '',
    placeholder: '',
    value: '',
    required: true,
    disabled: false,
    readOnly: false,
    autoFocus: false,
    onChange: () => {},
    classes: null,
    marginTop: 20,
}

Input.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    readOnly: PropTypes.bool,
    autoFocus: PropTypes.bool,
    onChange: PropTypes.func,
    classes: PropTypes.object,
    marginTop: PropTypes.number,
}

export default withStyles(styles)(Input)