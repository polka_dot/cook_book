import React from 'react'
import ButtonMaterial from '@material-ui/core/Button'
import {withTheme} from '@material-ui/core'
import PropTypes from 'prop-types'

const Button = ({
    children,
    small,
    type,
    disabled,
    onClick,
    variant,
    fullWidth,
    theme,
}) => {
    const style = {
        root: {
            fontSize: small ? 10 : 13,
            background: theme.palette.stateColor.defaultColor.bgPrimary,
            opacity: disabled && .5,
            color: theme.palette.stateColor.defaultColor.bgWhite,
            padding: '10px 20px',
            marginTop: 20,
        }
    }
    return (
        <ButtonMaterial
            style={style.root}
            type={type}
            disabled={disabled}
            onClick={onClick}
            variant={variant}
            fullWidth={fullWidth}
        >
            {children}
        </ButtonMaterial>
    )
}

Button.defaultProps = {
    small: false,
    type: 'submit',
    disabled: false,
    onClick: () => {},
    variant: 'contained',
    fullWidth: true,
}

Button.propTypes = {
    children: PropTypes.any.isRequired,
    small: PropTypes.bool,
    type: PropTypes.string,
    disabled: PropTypes.bool,
    onClick: PropTypes.func,
    variant: PropTypes.string,
    fullWidth: PropTypes.bool,
    theme: PropTypes.object.isRequired,
}

export default withTheme(Button)