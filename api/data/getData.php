<?php
    $conn = '';
    include('../lib/database.php');

    $sql = 'SELECT * FROM Przepisy';

    $result = $conn->query($sql);
    $data = array();
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($data, $row);
        }
        echo json_encode($data);
    }
